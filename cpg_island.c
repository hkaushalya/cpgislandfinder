#include "cpg_island.h"

char *getseq (char *input_file, char *buffer, int *Nmax, int lines_to_skip, int *intsym)
/* This function reads the sequence, determines the no. of valid symbols and then reads in the sequence */

{
  int i, m, n, I;
  long buffersize = 1000000;
  char *seq;
  FILE *fin;
   

  fin = fopen (input_file, "r");
   
        for (i=0; i<lines_to_skip; i++)
        {
		    fgets(buffer,buffersize,fin);
		}

  (*Nmax) = 0;

	/* Determine sequence length to allocate memory to the seq pointer */
 	
    while (!feof(fin))
      {
	    I = fread (buffer, sizeof (char), buffersize, fin);
  
        for (i=0; i<I; i++)
    	if ((m = intsym[(int)(buffer[i])]) >= 0)                        
	  	{
	   		(*Nmax)++;	
		}
    }

  fclose (fin);  
   
  seq = allocchar1 ((*Nmax)+1);
      
  fin = fopen (input_file, "r");


	// Throw away lines_to_skip no. of lines here (usually only the first line)
	for (i=0; i<lines_to_skip; i++)
        {
	  //            fscanf(fin,"%s", tbuff);
		    fgets(buffer,buffersize,fin);
		}

/* Read in the actual sequence here */
	
  n = 0;
  while (!feof(fin))
    {
      I = fread (buffer, sizeof (char), buffersize, fin);

      for (i=0; i<I; i++)
	if ((m = intsym[(int)(buffer[i])]) >= 0)                        // convert to lower
        {
            seq[n] = buffer[i];
            n++;
        }
    }
  seq[(*Nmax)] = '\0';
  fclose (fin);
  return seq;
}

char *complementseq (long N, char *seq, int *intsym)
/* This function reverse complements the sequence and returns it in the seq pointer itself */
{
  int m;
  long  n, ny;
  const char cnt[]="tgca", nt[]="acgt";
  char *compseq;
      
  compseq = allocchar1 (N);

  for (n=0; n<N; n++)
    {
        m = intsym[(int)(seq[n])];
        ny = N-n-1;
        compseq[ny] = cnt[m]; 
    }

  return compseq;
}

double cpgrelated (char *seq, int Nmax, int sliding_window, int (*pos_max), double *cpg_scores)
{

	/* Receives the sequence pointer, the length of the sliding window to be used in
	   calculation of the position of the first cpg window and the maximum cpg window with maximum cpg count */

  int n, count = 0, count_max;
  double max_perc;
	
  for (n=0; n<sliding_window; n++)
    if (strncmp (seq+n, "cg", 2) == 0)
      count ++;

  count_max = count;
  (*pos_max) = 0;			// initialize the variables
  cpg_scores[0] = (double)(count)/2;
	
  for (n=1; n<Nmax-200; n++)
    {
      if (strncmp (seq+n-1, "cg", 2) == 0)			// decrease count in the window if there was a CG just before it
        count --;
      if (strncmp (seq+n+sliding_window-1, "cg", 2) == 0)
        count ++;									// increase count if there is a CG at the end
	  
      if (count > count_max)
        {
          count_max = count;
          (*pos_max) = n;
        }
	  cpg_scores[n]=(double)(count)/2;
    }
	max_perc = (double)(count_max)/2;
    return max_perc;
}


int main(int argc, char **argv)
{
  int Nmax, pos_max, rev, lines_to_skip = 1, *intsym;
  char *buffer, *seq, output_file[25], *input_file;
  double *cpg_scores, max_perc;
  FILE *fout;

  if (argc < 2)
    {
		return 1;
    }

	
  input_file = argv[1];
	
  sprintf (output_file, "%s_output", input_file);

  intsym = allocint1(256);

	
  for(int m=0;m<125;m++)
  {
    intsym[m] = -1;
  }

    intsym[(int)('a')] = 0;    
    intsym[(int)('A')] = 0;
    intsym[(int)('c')] = 1;
    intsym[(int)('C')] = 1;
    intsym[(int)('g')] = 2;
    intsym[(int)('G')] = 2;
    intsym[(int)('t')] = 3;
    intsym[(int)('T')] = 3;


	// get sequence

	buffer = allocchar1 (1000001);
	buffer[1000000] = '\0';
	seq = getseq (input_file, buffer, &Nmax, lines_to_skip, intsym);

  
    // allocate memory for cpg_scores;
    cpg_scores = allocdouble1(Nmax-200);

	fout = fopen(output_file, "w");
	
  
	for (rev=0;rev<2;rev++)
	{
	  if (rev == 1) 
	  {
		seq = complementseq (Nmax, seq, intsym);
	  }
		  fprintf(fout, "XXXX\n");

	  
	  max_perc = cpgrelated (seq, Nmax, 200, &pos_max, cpg_scores);


	    fprintf(fout, "%.2f\n", max_perc);
	  	fprintf(fout, "%3d\n", pos_max);
	
		for (int i=0; i<Nmax-200; i++) {  
	  		fprintf(fout, "%.2f\n",cpg_scores[i]);
		}
	  
	}	  
	fclose(fout);
	
	free(seq);
	free(intsym);
	free(buffer);
    free(cpg_scores);
return 0;
}

