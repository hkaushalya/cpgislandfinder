#include <ctype.h>
#include <float.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream.h>
#include <pthread.h>
#include <unistd.h>

#include "mem_modules.c"

extern char *allocchar1 (long A);
extern double *allocdouble1 (long A);
extern double **allocdouble2 (long A, long B);
extern double ***allocdouble3 (long A, long B, long C);
extern double ***readpwmfile (const char *inname, int *N, int *M, int *L);
extern FILE **allocfile1 (long A);
extern int *allocint1 (long A);
extern int **allocint2 (long A, long B);
extern short *allocshort1 (long A);
extern void fileopenerror (FILE *fin, const char *str);
extern void freedouble2 (long A, long B, double **x);
extern void freedouble3 (long A, long B, long C, double ***x);
extern void freeint2 (long A, long B, int **x);
extern void printfdouble1 (int A, double *p, const char *s, double fac);
extern void printfdouble2 (int A, int B, double **p, const char *s, double fac);
extern void printfdouble3 (int A, int B, int C, double ***p, const char *s, double fac);


extern char *getseq (char *input_file, char *buffer, int *Nmax, int lines_to_skip, int *intsym);
extern char *complementseq (long N, char *seq, int *intsym);
extern double cpgrelated (char *seq, int Nmax, int sliding_window, int (*pos_max), double (*cpg_scores));

